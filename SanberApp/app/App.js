import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import VideoItem from './components/videoItem';
import data from './data.json';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image
            source={require('./images/logo.png')}
            style={{width: 98, height: 22}}
          />
          <View style={styles.rightNav}>
            <TouchableOpacity>
              <Image
                source={require('./images/search.png')}
                style={styles.navItem}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                source={require('./images/account-circle.png')}
                style={styles.navItem}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.body}>
          <FlatList
            data={data.items}
            renderItem={video => <VideoItem video={video.item} />}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={() => (
              <View style={{height: 0.5, backgroundColor: '#E5E5E5'}} />
            )}
          />
        </View>
        <View style={styles.tabBar}>
          <TouchableOpacity>
            <View style={styles.tabItem}>
              <Image source={require('./images/home.png')} />
              <Text style={styles.tabTitle}>Home</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.tabItem}>
              <Image source={require('./images/whatshot.png')} />
              <Text style={styles.tabTitle}>Trending</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.tabItem}>
              <Image source={require('./images/subscriptions.png')} />
              <Text style={styles.tabTitle}>Subscriptions</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.tabItem}>
              <Image source={require('./images/folder.png')} />
              <Text style={styles.tabTitle}>Library</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rightNav: {
    flexDirection: 'row',
  },
  navItem: {
    marginLeft: 15,
  },
  body: {
    flex: 1,
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#e5e5e5',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabTitle: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 5,
  },
});
